var originalRate = {};
var countryCurrency;
var currencyCountryArr = [];
var countrySelector = document.querySelector("#countrySelector");
init();
var processId = window.setInterval(init,3600000);

document.addEventListener('DOMContentLoaded',function() {
    document.querySelector('#countrySelector').onchange = changeEventHandler;
},false);

//-------------------------------------------------LISTENERS-------------------------------------------------

var baseNum = document.querySelector("#baseNum");					// Amount input
var newRateList = [];												// Store the calculated rates
var keyList = [];													// Store the keys
baseNum.addEventListener("change", function(){
	newRateList = [];
	let curVal = Number(baseNum.value);
	if(curVal < 1){
		curVal = 1;
		baseNum.value = curVal;
		alert("The lowest value is 1");
	}
	if(localStorage != undefined){
		$.each(localStorage, function(key, value){
			keyList.push(key);
			let updatedVal = value * curVal;
			if(updatedVal >= 1000){
				updatedVal = updatedVal.toFixed(1);
			}else{
				updatedVal = updatedVal.toFixed(4);
			}
			newRateList.push(updatedVal);
		});
	}else{
		$.each(originalRate.rates, function(key, value){
			keyList.push(key);
			let updatedVal = value * curVal;
			if(updatedVal >= 1000){
				updatedVal = updatedVal.toFixed(1);
			}else{
				updatedVal = updatedVal.toFixed(4);
			}
			newRateList.push(updatedVal);
		});
	}
	updateTable();
});

var currencyLocator = document.querySelector("#currencyLocator");
currencyLocator.addEventListener("change", function(){
	if(localStorage != undefined){
		$.each(localStorage, function(key, value){
			let input = currencyLocator.value;	
			input = input.toUpperCase();
			if(input === key){
				window.location.href = "#"+key;
			}
		});
	}else{
		$.each(originalRate.rates, function(key, value){
			let input = currencyLocator.value;		
			input = input.toUpperCase();
			if(input === key){
				window.location.href = "#"+key;
			}
		});
	}
});

//-------------------------------------------------FUNCTIONS-------------------------------------------------

function initRate(data){
	Object.keys(data).sort().forEach(function(key) {
  		if(key === "rates"){
  			originalRate.rates = {};
  			Object.keys(data.rates).sort().forEach(function(key){
  				originalRate.rates[key] = data.rates[key];
  			});
  		}else{
  			originalRate[key] = data[key];
  		}
	});
}

function objToTable(){
	$("#list").html("");
	dropSelectElement();
	baseNum.value = 1;
	$.each(originalRate.rates, function(key, value){
		if(typeof(Storage) !== undefined){
			if(localStorage.getItem(key) === null || localStorage.getItem(key) !== value)
				localStorage.setItem(key, value);
		}else{
			console.log("Broswer does not support we storage!");
		}
		$("#list").append("<div id=" + key + ">" + "<div class='imgDisplay'></div>" + "<div class='currencyDisplay'>" + key + "</div>" + "<div class='rateDisplay'>" + value.toFixed(4) + "</div>" + "</div>");
		$("div#" + key).attr("class", "square show");
		getCountryFlag(key);
	});
	squareAction();
}

function updateTable(){
	$.each(keyList, function(index, value){
		$("#list div#" + value + " div.rateDisplay").text(newRateList[index]);
		getCountryFlag(value);
	});
}

function getCountryFlag(key){
	let imgVal = currencyCountryArr[key];
	if (key === "EUR") {
		imgVal = "eu";
	}
	if(key === "GBP"){
		imgVal = "gb";
	}
	if(key === "NZD"){
		imgVal = "nz";	
	}
	if(key === "USD"){
		imgVal = "us";		
	}
	let imgSrc = 'assets/img/flag/128/' + imgVal +'.png';
	$("#list div#"+ key +" div.imgDisplay").html("<img>");
	if($("#list div#"+ key +" div.imgDisplay img").prop("src") != undefined){
		$("#list div#"+ key +" div.imgDisplay img").attr("src", imgSrc);
	}
	$("#list div#"+ key +" div.imgDisplay img").on("error", function(){
		$("#list div#"+ key +" div.imgDisplay img").removeProp("src");
		$("#list div#"+ key).removeClass("show");
		$("#list div#"+ key).addClass("remove");
	});
}


function countryCurrencyConvert(){
	$.each(countryCurrency, function(key, value){
    	currencyCountryArr[value] = key.toLowerCase();
	});
}

function init(){
	$.getJSON("https://api.myjson.com/bins/1djwh0", function(data){
		countryCurrency = data;
		countryCurrencyConvert();
		countrySelectorInit();
		initModalContent();
	}).done(function(){
		$.getJSON("https://api.exchangeratesapi.io/latest?base=HKD", initRate).done(objToTable).fail(function(){
			alert("Cannot fetch rates!");
		});
	}).fail(function(){
		alert("Cannot fetch countryCode");
	});
}

function squareAction(){
	$("#list div.square").each(function(){
		this.addEventListener("click",function(){
			this.classList.remove("show");
			this.classList.add("hide");
			let temp = document.createElement("option");
			let key = this.id;
			temp.value = key;
			temp.text = key;
			countrySelector.add(temp);
		})
	});
}

function countrySelectorInit(){
	let temp = document.createElement("option");
	temp.value = "default";
	temp.text = "---Select elements in this list to show the hiding items---";
	countrySelector.add(temp);
}

function changeEventHandler(event) {
    $("#"+event.target.value).removeClass("hide");
    $("#"+event.target.value).addClass("show");
    countrySelector.remove(event.target.selectedIndex);
}

function dropSelectElement(){
	$.each(countrySelector.options, function(index, value){
		if(index != 0){
			countrySelector.remove(index);
		}
	});
}

function initModalContent(){
	if(localStorage != undefined){
		$.each(localStorage, function(key, value){
			if(key.length <= 3 && key !== "key"){
				$.getJSON("https://restcountries.eu/rest/v2/currency/" + key, function(data){
					$.each(data, function(index, value){
						var comp = currencyCountryArr[key].toUpperCase();
						if(comp === value.alpha2Code){
							let content = "<p><span class='modalKey'>" + key +"</span><span class='modalVal'>" + this.name + "</span></p>";
							$(".modal-body").append(content);
						}
					});
				});
			}
		});
	}else{
		$.each(originalRate.rates, function(key, value){
			if(key.length <= 3 && key !== "key"){
				$.getJSON("https://restcountries.eu/rest/v2/currency/" + key, function(data){
					$.each(data, function(index, value){
						var comp = currencyCountryArr[key].toUpperCase();
						if(comp === value.alpha2Code){
							let content = "<p><span class='modalKey'>" + key +"</span><span class='modalVal'>" + this.name + "</span></p>";
							$(".modal-body").append(content);
						}
					});
				});
			}
		});
	}
}


